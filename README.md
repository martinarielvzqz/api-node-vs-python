# COMPARATIVA API REST: node vs python

## **ORM**

Primeramente se comparará el ORM en [node](https://nodejs.org/es/) y [python](https://www.python.org/).

Para Node se utiliza [Sequelize](https://sequelize.org/) (otra alternativa interesante podria haber sido  [TypeORM](https://typeorm.io)).

Para Python se utiliza el ORM de [Django](https://www.djangoproject.com/).

Al principio por simplicidad de configuración se utiliza [Sqlite](https://www.sqlite.org/index.html) en ambos proyectos, mas adelante se modificara esto para utilizar [MySQL](https://www.mysql.com/). (y ver mejor el tema de migraciones)


## **API**

Una vez terminado la etapa de modelado comenzaremos a implementar la API REST creando los endpoints necesarios para realizar un CRUD de estos modelos.

Para node se escoge [Express](https://expressjs.com/es/)

Para python se usa aplicación [Django REST Framework](https://www.django-rest-framework.org/) que corre sobre Django

